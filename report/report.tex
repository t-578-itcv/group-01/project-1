
\documentclass[]{IEEEtran}
% If IEEEtran.cls has not been installed into the LaTeX system files,
% manually specify the path to it like:
% \documentclass[conference]{../sty/IEEEtran}

% Your packages go here
\usepackage[utf8]{inputenc}
\usepackage{mathtools}
\usepackage{blkarray} %
\usepackage{listings} %Package to include

\markboth{T-578-ITCV Computer Vision}{}

\begin{document}

  \title{Project 1 - Edge Detection}

  \author{
    \IEEEauthorblockN{Casimir Wallwitz, Leith Hobson and Stefán Laxdal}

    \thanks{casimir21, leith21, stefanl20}{casimir21, leith21, stefanl20}
  }

  \maketitle



  \begin{abstract}

    In this paper we describe the process through which we generate different filters for edge detection and use various images processed with these filters to feed different machine learning algorithms and detect the edges based on that output. We did not manage to improve upon or match our benchmark, the Canny edge detector. This was more about experimenting with different filters and algorithms, and with the time restrictions, we did not necessarily aim to produce the best results.
    
  \end{abstract}

  \section{Introduction}
  \label{sec:introduction}

  Edge detection in photographs is a fundamental problem of computer vision and one for which many techniques have been produced over the decades. In this project we utilised the first and second derivatives of a simple Gaussian kernel to estimate the gradient of a pixel at any given point and from there we extract information about the ascending and descending properties of edges in a given image.


  \section{Body}
  \label{sec:body}
  
    \subsection*{Convolution}
    \label{subsec:convolution}
    
    
    In order to perform the necessary convolutions, we have written a convolution function which receives an input array and a kernel. The function starts by flipping the kernel and then creates an output array with the dimensions of the input array, but filled initially with zero values.
    The function then traverses through every position in the output array for which data can be calculated. For each position a new value is calculated based on the kernel and the cells surrounding the current position in the input array.
    
    \begin{lstlisting}[language=Python]
    for x in range(lD, imgHeight - lD):
      for y in range(lD, imgWidth - lD):

        pixelSum = np.sum(np.multiply(
          kernel,
          inArr[
            x - lD:x + lD + 1,
            y - lD:y + lD + 1
          ]
        ))

            outArr[x, y] = pixelSum
    return outArr
    \end{lstlisting}

    \newpage
    \subsection*{Filter Bank}
    \label{subsec:filterBank}

    We have implemented our filter bank generation through a dedicated filter generation class. This class begins by generating three 
    elongated Gaussian kernels with different dimensions. We began by experimenting with the kernel sizes, initially using smaller kernels
    with values evenly spaced. All of our kernels were off odd dimensions to enable anchoring the center, while we found the even dimensions which we were experimenting with tended to act in an unstable manner when stretched and rotated. Further more the closely related sizes produced results
    that were very similar to one another. We therefore decided to try producing large kernels of various spacing and then crop their size down
    in the rotation process so that their distributions of values was roughly the same across the filters.
    Three Gaussian kernels, with the dimensions 13x13, 17x17 and 23x23, are generated.
    Each of these kernels is convolved with a 3x3 Sobel kernel in order to produce the first derivative.
    These output kernels are then convolved again with the Sobel kernel to produce the second derivative.

    Once this full set of kernels has been generated, we rotate each of them through 0$^\circ,$ 30$^\circ,$ 60$^\circ,$ 90$^\circ,$ 120$^\circ,$ and 150$^\circ,$ in order to generate the full bank of 12 kernels per size, totaling 36 kernels.
    \newline
    \newline
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.135]{images/first.png}
        \caption{Various rotations of the first derivative.}
        \label{fig:my_label}
    \end{figure}
    \newline
    \newline
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.135]{images/second.png}
        \caption{Various rotations of the second derivative.}
        \label{fig:my_label}
    \end{figure}

    \newpage
    \subsection*{Classification Examples}
    \label{subsec:classificationExamples}
    From the examples we computed it is clear that all classifiers tend to misclassify non-edges as edges a lot. The pictures come out as overly bright and the edges are large surfaces. More training examples helped with this slightly, but some tuning of hyperparameters would have been required which we ran out of time for. The accuracies fluctuate heavily between 60-80\%, with the occasional outliers at around 40\%. The precision-recall curve looks about as you would expect: Very high precision for darker pixels, with a sudden drop after anything that is not completely black.
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.55]{images/precision_recall.jpg}
        \caption{Precision-recall curve for one of the images}
        \label{fig:my_label}
    \end{figure}
    \\
    The individual Classifiers, filter sizes or hyperparameters were not sufficiently tested by us to produce any noticeable differences in the results. Some minor differences in noisiness could be seen between approaches, but most of these we attribute to randomness. \\
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.15]{images/nn-42049-output.jpg}
        \includegraphics[scale=0.15]{images/svm-42049-output.jpg}
        \includegraphics[scale=0.15]{images/nn-175043-output.jpg}
        \caption{The good, the bad and the ugly: \\ Neural Network, Support Vector Machine and Neural Network again all with filters of size 13 as inputs.}
        \label{fig:my_label}
    \end{figure}
    \\
    Another uncertainty was added by the fact that the ground truth was also generated by a Canny edge detector itself. While the output was often good, we were training the models with imperfect data.

    \newpage
    \subsection*{Feature Impact}
    The most prominent difference between the first and second derivative Gaussian filters that we noted is in how they respond to the edges 
    they encounter. We observed that the first derivative filters populate the edge itself with rising and falling intensities of white pixels, 
    similarly to how we might expect a human to draw over the edges, while, on the other hand the second derivative filters imprint two such 
    regions outlining each edge, with the actual edge being the black center with its thickness corresponding to the prominence of the edge 
    identified by the filter.
    
    Through the use of the three different sized Gaussian kernels we noted that in the cases of both the first and second 
    derivative the edges become less clearly defined as the dimensions of the kernel grow. The larger kernels also 
    provide results where the more prominent edges are captured while smaller less defined edge information is lost, this has the positive 
    impact of cleaning the image of noise.
    
    When the outputs of the various kernels are combined and then normalised, the images filtered by the first and second derivatives closely resemble the ones produced
    by convolving the image with only the second derivative filters. The difference is less noticeable when using the smaller 
    kernel but more prominent on the larger kernels, the difference being a slight spread of white pixels in the center of the edges
    found by the second derivative filter. In the case of results produced by both the first and second derivative filters we did encounter
    outliers where the resulting image was not similar to its counterparts produced from the first or second derivative respectively. 
    In these cases the resulting image would have entire sections shaded slightly in gray values, the noise of the image having reacted
    and been increased by both filters respectively before being combined in the new result.
    \label{subsec:featureImpact}
    \newline
    \label{86000-f13.jpg} 
    \begin{figure}[h]
        \includegraphics[width=0.2\textwidth]{images/86000-f13.jpg}
        \includegraphics[width=0.2\textwidth]{images/86000-s13.jpg}
      \caption{First and second derivative at 13X13}
      \label{fig:gull}
    \end{figure}
    \label{86000-f13.jpg} 
    \begin{figure}[h]
        \includegraphics[width=0.2\textwidth]{images/86000-s23.jpg}
        \includegraphics[width=0.2\textwidth]{images/86000-b23.jpg}
      \caption{Second derivative and both first and second mixed at 23X23}
      \label{fig:gull}
    \end{figure}



  \newpage
  \section{Conclusion}
  \label{sec:conclusion}
    
  In the case of using the Sobel-Gaussian filter the larger kernels lost some information in the smaller edges but also tended towards less noise than the smaller kernels. Visually, we found the smaller kernels to produce the most appealing results. However when training different classifiers using our results we found that the different classifiers produced very similar results when tested. Our models produce the general outlines of the most prominent edges in pictures that lack a large amount of clutter, but fail on lesser edges. This could potentially be remedied by using smaller kernels. 

  \section{Contribution}
  \label{sec:contribution}

    \begin{equation*}
      \begin{blockarray}{*{5}{c} l}
        \begin{block}{*{5}{>{$\footnotesize}c<{$}} l}
          Convolution & Filter Bank & Classifier & Discussion & Report & \\
        \end{block}
        \begin{block}{[*{5}{c}]>{$\footnotesize}l<{$}}
          31 & 31 & 40 & 33 & 33 & Casimir \\
          36 & 33 & 20 & 33 & 33 & Leith \\
          33 & 36 & 20 & 34 & 34 & Stefán \\
        \end{block}
      \end{blockarray}
    \end{equation*}
    

\end{document}


