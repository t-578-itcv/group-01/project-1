import sys
import os
import cv2 as cv
import numpy as np


def get_picture_info(picture_id, train=True):
    DIR = sys.path[0]
    t = ('train' if train else 'test')
    original_picture = cv.imread(DIR+"/../input/BSDS300/images/"+t+"/"+picture_id+".jpg", cv.IMREAD_GRAYSCALE) 
    canny_picture = cv.Canny(original_picture, 200, 200).flatten()
    f = cv.imread(DIR+"/../output/images/"+t+"/"+picture_id+"-f13.jpg", cv.IMREAD_GRAYSCALE) 
    s = cv.imread(DIR+"/../output/images/"+t+"/"+picture_id+"-s13.jpg", cv.IMREAD_GRAYSCALE) 
    b = cv.imread(DIR+"/../output/images/"+t+"/"+picture_id+"-b13.jpg", cv.IMREAD_GRAYSCALE) 


    res = {
            "original": original_picture,
            "y": canny_picture,
            "X": np.array(list(zip(np.array(f).flatten(), np.array(s).flatten(), np.array(b).flatten()))),
            "xDim": f.shape,
            "f": f,
            "s": s,
            "b": b
    } 

    return res

if __name__ == "__main__":
    res = get_picture_info("138032")
