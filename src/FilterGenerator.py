import numpy as np
import cv2 as cv
from scipy import ndimage
from matrixVisualisation import DisplayMatrix
from convolution import Convolution
from Gaussian import gaussian_kernel


class FilterGenerator:
    def __init__(self):
        self.convolution = Convolution()
        self.display = DisplayMatrix()
        self.kernel = np.array([
            [-1, -2, -1],
            [0, 0, 0],
            [1, 2, 1]
        ])
        self.first = []
        self.second = []
        self.first_13 = None
        self.first_17 = None
        self.first_23 = None
        self.second_13 = None
        self.second_17 = None
        self.second_23 = None

    def generate_rotated_kernels(self, matrix, first=True):
        res = []
        for rot in [0, 30, 60, 90, 120, 150]:
            res.append(self.rotate(matrix, rot, first))
        return res

    def rotate(self, image, angle, first=True):
        img = np.pad(image, 4, mode="edge")
        rotated = ndimage.rotate(img, angle, reshape=False)
        if not first:
            rotated = rotated[3:-3, 3:-3]
        else:
            rotated = rotated[4:-4, 4:-4]

        return rotated

    def generate(self):
       # small_gauss = gaussian_kernel(13, 1.7)
       # medium_gauss = gaussian_kernel(17, 2.2)
       # large_gauss = gaussian_kernel(23, 3.5)

        small_gauss = gaussian_kernel(13, 1.7)
        medium_gauss = gaussian_kernel(17, 2.2)
        large_gauss = gaussian_kernel(23, 3.5)

        # Create first and second derivatives
        self.first.append(self.convolution.convolution(
            small_gauss, self.kernel))
        self.first.append(self.convolution.convolution(
            medium_gauss, self.kernel))
        self.first.append(self.convolution.convolution(
            large_gauss, self.kernel))

        for m in self.first:
            self.second.append(self.convolution.convolution(m, self.kernel))

        # Generate rotations
        self.first_13 = self.generate_rotated_kernels(self.first[0])
        self.first_17 = self.generate_rotated_kernels(self.first[1])
        self.first_23 = self.generate_rotated_kernels(self.first[2])
        self.second_13 = self.generate_rotated_kernels(
            self.second[0], first=False)
        self.second_17 = self.generate_rotated_kernels(
            self.second[1], first=False)
        self.second_23 = self.generate_rotated_kernels(
            self.second[2], first=False)

    def show(self, matrix):
        self.display.visualiseMatrix(matrix)

    def display_first(self, fileName=None):
        self.display.visualiseMatrix(
            matrices=self.first_17, display="bilinear", fileName=fileName)

    def display_second(self, fileName=None):
        self.display.visualiseMatrix(
            matrices=self.second_17, display="bilinear", fileName=fileName)


if __name__ == "__main__":
    fg = FilterGenerator()
    fg.generate()
    kernels = fg.second_23

    c = Convolution()
    img = cv.imread('../images/BSDS300/images/test/24077.jpg',
                    cv.IMREAD_GRAYSCALE)

    for k in kernels:
        res = Convolution().convolution(img, k)
        cv.imshow('', c.normalize(res))
        cv.waitKey(0)
