import numpy as np
import cv2 as cv


class Convolution:
    def convolution(self, inArr, kernel=None):

        # If there is no mask, simply return the input
        if kernel is None:
            print("No mask provided, returning inArr")
            return inArr

        # get mask and image width and height
        [maskHeight, maskWidth] = np.shape(kernel)
        [imgHeight, imgWidth] = np.shape(inArr)

        # flip the kernel
        kernel = np.flipud(np.fliplr(kernel))

        # create blank array to return
        outArr = np.zeros((imgHeight, imgWidth))

        outHeighLostBorder = (maskHeight // 2)
        outWidthLostBorder = (maskWidth // 2)
        for x in range(outHeighLostBorder, imgHeight - outHeighLostBorder):
            for y in range(outWidthLostBorder, imgWidth - outWidthLostBorder):

                pixelSum = np.sum(np.multiply(
                    kernel,
                    inArr[
                        x - outHeighLostBorder:x + outHeighLostBorder + 1,
                        y - outWidthLostBorder:y + outWidthLostBorder + 1
                    ]
                ))

                outArr[x, y] = pixelSum
        return outArr

    def normalize(self, inArr):
        max = np.max(inArr)
        min = np.min(inArr)
        [arrHeight, arrWidth] = np.shape(inArr)
        newArr = np.zeros((arrHeight, arrWidth))

        for i in range(arrHeight):
            for j in range(arrWidth):
                newArr[i, j] = ((inArr[i, j] - min) / (max - min)) * 255

        newArr = np.round(newArr).astype(np.uint8)

        return newArr
