import numpy as np
from matrixVisualisation import DisplayMatrix

# modified from https://towardsdatascience.com/canny-edge-detection-step-by-step-in-python-computer-vision-b49c3a2d8123
def gaussian_kernel(size, sigma=1):
    size = int(size) // 2
    x, y = np.mgrid[-size:size+1, -size:size+1]
    normal = 1 / (2.0 * np.pi * sigma**2)
    g =  np.exp(-((x**2 * 3 + y**2) / (2.0*sigma**2))) * normal
    return g


if __name__ == "__main__":
    g = gaussian_kernel(5) 


