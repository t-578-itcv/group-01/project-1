#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm


class DisplayMatrix:
    def __init__(self):
        pass

    def visualiseMatrix(self, matrix=None, matrices=None, display="dual", fileName=None, figSize=(32, 12)):

        # Backwards compatibility with initial implementation
        if matrices is None:
            if matrix is not None:
                matrices = [matrix]
            else:
                matrices = [
                    np.matrix(data=((1, 0, -1), (2, 0, -2), (1, 0, -1)))]

        fig = plt.figure(figsize=figSize)

        count = matrices.__len__()
        if display.__contains__("dual"):
            count = count*2

        rows = max(count//6, 1)
        columns = min(count, 6)

        position = 1

        for matrix in matrices:

            if display.__contains__("dual") or display.__contains__("nearest"):
                axis = fig.add_subplot(rows, columns, position)
                axis.imshow(matrix, interpolation='nearest', cmap=cm.viridis)
                position += 1

            if display == "dual" or display == "bilinear":
                axis = fig.add_subplot(rows, columns, position)
                axis.imshow(matrix, interpolation='bilinear', cmap=cm.viridis)
                position += 1

        if(fileName is None):
            plt.show()
        else:
            plt.savefig(fileName)


if __name__ == "__main__":
    DisplayMatrix().visualiseMatrix()
