#!/usr/bin/env bash

echo "Creating in/output directories"
mkdir -p input/

mkdir -p output/result/
mkdir -p output/images/kernels/
mkdir -p output/images/train/
mkdir -p output/images/test/

cd input/

echo "Downloading images:"
curl https://www2.eecs.berkeley.edu/Research/Projects/CS/vision/grouping/segbench/BSDS300-images.tgz > images.tgz



echo "Unzipping images:"
tar -xvzf images.tgz

echo "Data preperation complete"
