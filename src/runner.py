#!/bin/python3

import multiprocessing
import random
import sys
import time

# importing the opencv module
import numpy as np
import cv2

from FilterGenerator import FilterGenerator
from convolution import Convolution
from matrixVisualisation import DisplayMatrix


class Runner():
    def __init__(self):
        self.numImages = 5
        self.imgScaleFactor = 1

        self.convolution = Convolution()

        # kernels
        self.kernelSobelY = np.array([[-1, -2, -1],
                                      [0, 0, 0],
                                      [1, 2, 1]])

        self.kernelSobelX = np.array([[-1, 0, 1],
                                      [-2, 0, 2],
                                      [-1, 0, 1]])

        self.fg = FilterGenerator()
        self.fg.generate()

    def saveImage(self, path, image):

        # dsize
        outputSize = (round(image.shape[1] * self.imgScaleFactor),
                      round(image.shape[0] * self.imgScaleFactor))

        if (self.imgScaleFactor != 1):
            image = cv2.resize(image, outputSize,
                               interpolation=cv2.INTER_NEAREST)

        cv2.imwrite(path, image)

    def process_picture(self, line, imgFolder):
        thisStart = time.time()
        p = sys.path[0] + '/../input/BSDS300/images/' + imgFolder + \
            str(line).strip()+'.jpg'
        img = cv2.imread(p, cv2.IMREAD_GRAYSCALE)
        print("Opening ", line.strip())
        f13 = 0
        f17 = 0
        f23 = 0
        s13 = 0
        s17 = 0
        s23 = 0
        b13 = 0
        b17 = 0
        b23 = 0

        for f in self.fg.first_13:
            f13 += (self.convolution.convolution(img, f)) ** 2
        f13n = self.convolution.normalize(f13)
        self.saveImage(sys.path[0] + '/../output/images/' + imgFolder +
                       line.strip() + "-f13.jpg", f13n)

        for f in self.fg.first_17:
            f17 += (self.convolution.convolution(img, f)) ** 2
        f17n = self.convolution.normalize(f17)
        self.saveImage(sys.path[0] + '/../output/images/' + imgFolder +
                       line.strip() + "-f17.jpg", f17n)

        for f in self.fg.first_23:
            f23 += (self.convolution.convolution(img, f)) ** 2
        f23n = self.convolution.normalize(f23)
        self.saveImage(sys.path[0] + '/../output/images/' + imgFolder +
                       line.strip() + "-f23.jpg", f23n)

        for f in self.fg.second_13:
            s13 += (self.convolution.convolution(img, f)) ** 2
        s13n = self.convolution.normalize(s13)
        self.saveImage(sys.path[0] + '/../output/images/' + imgFolder +
                       line.strip() + "-s13.jpg", s13n)

        for f in self.fg.second_17:
            s17 += (self.convolution.convolution(img, f)) ** 2
        s17n = self.convolution.normalize(s17)
        self.saveImage(sys.path[0] + '/../output/images/' + imgFolder +
                       line.strip() + "-s17.jpg", s17n)

        for f in self.fg.second_23:
            s23 += (self.convolution.convolution(img, f)) ** 2
        s23n = self.convolution.normalize(s23)
        self.saveImage(sys.path[0] + '/../output/images/' + imgFolder +
                       line.strip() + "-s23.jpg", s23n)

        b13 = f13 + s13
        b13n = self.convolution.normalize(b13)
        self.saveImage(sys.path[0] + '/../output/images/' + imgFolder +
                       line.strip() + "-b13.jpg", b13n)

        b17 = f17 + s17
        b17n = self.convolution.normalize(b17)
        self.saveImage(sys.path[0] + '/../output/images/' + imgFolder +
                       line.strip() + "-b17.jpg", b17n)

        b23 = f23 + s23
        b23n = self.convolution.normalize(b23)
        self.saveImage(sys.path[0] + '/../output/images/' + imgFolder +
                       line.strip() + "-b23.jpg", b23n)

        print('Image complete, total time: ', round(
            time.time() - self.startTime), ' seconds')

    def main(self):

        self.startTime = time.time()

        self.fg.display_first(
            sys.path[0] + '/../output/images/kernels/first.svg')

        self.fg.display_second(
            sys.path[0] + '/../output/images/kernels/second.svg')

        self.fg.display_first(
            sys.path[0] + '/../output/images/kernels/first.png')

        self.fg.display_second(
            sys.path[0] + '/../output/images/kernels/second.png')

        imgCount = 0

        with open(sys.path[0] + '/../input/BSDS300/iids_train.txt') as f:

            imageIdsFull = []
            for line in f:
                imageIdsFull.append(line)
            f.close()  # close file

            trainImagesIds = []
            if self.numImages > imageIdsFull.__len__():
                self.numImages = imageIdsFull.__len__()

            for i in range(self.numImages):
                imId = random.choice(imageIdsFull)
                imageIdsFull.remove(imId)
                trainImagesIds.append(imId)

            # Print sorted highscores print to file
            file = open(sys.path[0] + '/../output/images/train/iids_used_train.txt',
                        'w')  # write to file
            file.writelines(trainImagesIds)
            file.close()  # close file

            processes = []
            for imId in trainImagesIds:
                imgCount += 1

                process = multiprocessing.Process(
                    target=self.process_picture, args=(imId, "train/",))
                processes.append(process)
            
            for process in processes:
                process.start()

            print('Load Time: ', round(time.time() - self.startTime), ' seconds')


        with open(sys.path[0] + '/../input/BSDS300/iids_test.txt') as f:

            imageIdsFull = []
            for line in f:
                imageIdsFull.append(line)
            f.close()  # close file

            testImagesIds = []
            if self.numImages > imageIdsFull.__len__():
                self.numImages = imageIdsFull.__len__()

            for i in range(self.numImages):
                imId = random.choice(imageIdsFull)
                imageIdsFull.remove(imId)
                testImagesIds.append(imId)

            # Print sorted highscores print to file
            file = open(sys.path[0] + '/../output/images/test/iids_output_test.txt',
                        'w')  # write to file
            file.writelines(testImagesIds)
            file.close()  # close file

            processes = []
            for imId in testImagesIds:
                imgCount += 1

                process = multiprocessing.Process(
                    target=self.process_picture, args=(imId, "test/",))
                processes.append(process)


            for process in processes:
                process.start()

            print('Load Time: ', round(time.time() - self.startTime), ' seconds')


            print('Total Time: ', round(time.time() - self.startTime), ' seconds')


if __name__ == "__main__":
    Runner().main()
