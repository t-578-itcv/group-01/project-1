#!/bin/python3

import time
import sys
import multiprocessing
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import PrecisionRecallDisplay
from sklearn.metrics import accuracy_score
from get_picture_info import get_picture_info


class Features:

    def __init__(self, classifier=(svm.SVC(), 'svm')):
        DIR = sys.path[0]
        self.clf = classifier[0]
        self.name = classifier[1]
        Xs = None
        ys = None
        for l in open(DIR+'/../output/images/train/iids_used_train.txt', 'r').readlines():
            training_set = get_picture_info(l.strip())
            X, y = self.equal_samples(training_set['X'], training_set['y'])
            if Xs is None:
                Xs = X
                ys = y
            else:
                Xs = np.append(Xs, X, axis=0)
                ys = np.append(ys, y)
        # svm takes way longer to train, so I'm just cutting it short here
        # also it gives me interesting results because the first pixels are darker on average
        # it's kind of cheating but at least it's something else
        if self.name == 'svm':
            if len(Xs) > 10000:
                Xs = Xs[:30000]
                ys = ys[:30000]
        self.X_train = Xs
        self.y_train = ys

    def train(self):
        print(f'training {self.name}...')
        start = time.time()
        self.clf.fit(self.X_train, self.y_train)
        print(f"training took {round(time.time() - start)} seconds")

    def equal_samples(self, X_raw, y_raw):
        smallest_class_amount = min(
            len([i for i in y_raw if i == 255]), len([i for i in y_raw if i == 255]))
        w_counter = 0
        b_counter = 0
        X, y = [], []

        for i in range(len(X_raw)):
            if w_counter >= smallest_class_amount and b_counter >= smallest_class_amount:
                break
            if w_counter >= smallest_class_amount and y_raw[i] > 200:
                continue
            if b_counter >= smallest_class_amount and y_raw[i] < 200:
                continue
            if y_raw[i] > 200:
                w_counter += 1
            else:
                b_counter += 1
            X.append(X_raw[i])
            y.append(y_raw[i])
        return (X, y)

    def recreate(self, data):
        pred = self.clf.predict(data['X'])

        print(f"accuracy {self.name}: {str(accuracy_score(data['y'], pred))}")
        #PrecisionRecallDisplay.from_predictions(data['y'], pred, pos_label=255)
        #plt.savefig(DIR+'/../output/result/'+self.name+'-'+l+'-precision_recall.jpg')

        recreated = pred.reshape(data['xDim'])
        cv.imwrite(DIR+'/../output/result/'+self.name+'-'+l+'-output.jpg', recreated)
        cv.imwrite(f"{DIR}/../output/result/{l}-canny.jpg", data['y'].reshape(data['xDim']))


if __name__ == '__main__':
    DIR = sys.path[0]

    models = [(make_pipeline(StandardScaler(), svm.SVC()), 'svm'), (RandomForestClassifier(n_estimators=80, max_depth=3), 'rf'), (MLPClassifier(hidden_layer_sizes=(3, 2)), 'nn')]
    for i in range(len(models)):
        start = time.time()
        F = Features(models[i])
        F.train()
        processes = []
        for l in open(DIR+'/../output/images/test/iids_output_test.txt', 'r').readlines():
            l = l.strip()
            data = get_picture_info(l, False)

            process = multiprocessing.Process(target=F.recreate, args=(data,))
            process.start()
        print(f"execution time for {models[i][1]} took {round(time.time() - start)} seconds")
    
    

